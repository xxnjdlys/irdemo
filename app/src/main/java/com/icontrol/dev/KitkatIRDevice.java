package com.icontrol.dev;

import android.content.Context;

/**
 * Created by sadieyu
 * Date: 15-4-12.
 * Time: 下午3:15
 */
public abstract class KitkatIRDevice{

    Context mContext;

    protected KitkatIRDevice(Context context) {
        mContext = context;
    }

    public native boolean isOpen();

    public native boolean sendIR(int var1, byte[] var2, int var3);

    public native void closeDevice();

    public native boolean openDevice(Context var1);
}
