package com.icontrol.dev;

import android.content.Context;
import android.util.Log;

/**
 * Created by sadieyu
 * Date: 15-4-12.
 * Time: 下午3:29
 */
public class IRUtils extends KitkatIRDevice {

    private static IRUtils sIRUtils;

    private static boolean isLoadLibraryed = true;

    static {
        try {
            System.loadLibrary("google_irdev");
//            System.loadLibrary("lattice_dev_usb");
//            System.loadLibrary("lattice_zte201402");
//            System.loadLibrary("skyworth_dev_spi");
//            System.loadLibrary("lattice_irs");
//            System.loadLibrary("lattice_tclfp");
//            System.loadLibrary("tiqiaa_m2m_uart");
//            System.loadLibrary("lattice_ztehs");
//            System.loadLibrary("tyd_dev");
//            System.loadLibrary("tn_i2c_dev");
//            System.loadLibrary("tiqiaa_dev_uart");
//            System.loadLibrary("ie_dev_uart");
//            System.loadLibrary("ie_dev_uart2");
//            System.loadLibrary("as43_irdev");
//            System.loadLibrary("at_i2c_dev");
//            System.loadLibrary("sirdev2");
//            System.loadLibrary("tiqiaa_cirdev");

            Log.v("SADIEYU", "Load libraries ...");
        } catch (UnsatisfiedLinkError localUnsatisfiedLinkError) {
            isLoadLibraryed = false;
            Log.v("SADIEYU", "Load libraries failed...");
        }
    }

    public boolean init() {
        return isLoadLibraryed;
    }

    protected IRUtils(Context context) {
        super(context);
    }

    public static IRUtils getInstance(Context context) {
        synchronized (IRUtils.class) {
            if (sIRUtils == null) {
                sIRUtils = new IRUtils(context);
            }
            return sIRUtils;
        }
    }

    public final boolean sendIRCode(int n, byte[] arrby, int n2) {
        return this.sendIR(n, arrby, 0);
    }

    public final void closeDevice() {
        synchronized (this) {
            this.closeDevice();
        }
    }

}
