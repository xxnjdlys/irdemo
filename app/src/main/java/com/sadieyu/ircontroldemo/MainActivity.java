package com.sadieyu.ircontroldemo;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.icontrol.dev.IRUtils;

public class MainActivity extends ActionBarActivity implements OnClickListener {

    public static final String POWER_KEY = "d8b95c9d9fd60c702bd0e3abba5e945f8afca5b13034166f5bf1386f106aa62a33f1af910d6a0b6470a83c637dc52899b61db795429e741c0c0d4d09f66464c07f14997941916fa3eaf797989442d212aa0f4f9433a4d01af19ff1ce7c267a600cf199217983914bd53f491f1fa42f83fb247a15f588dd8817961f0c9ff66c44";
    public static final String LEFT_KEY = "65ab3bf267031c5ae19147c6ab7bdfd0208885f2daf83bc099f60446d9fa72bade79754a517f3c1576ad34139fec0ad1ef815cb6023bc5db8ec9f8303c61b8e60e08c1f29617c902714045ad7b1fa02dbb6a596b8bf07bb15a33bdb201608f7e292d88d0b7252b9109ddf7ff0ac1eda4df2a28171fcbfb353857ebccc1df03ce";
    public static final String UP_KEY = "cde76f5b8880de803e76ba2123ddaf82dfd37a976f0181ef86a36d7e3d5842280553571695ac78bef2ba7f14944e341ea831e38c733450e3ed3771a4c0523fbd10ba459fc8aaba02650b6d14f1889dbda3d7ae16336fe8450e3318cd7c3a2300f301564a420e0e3d16c9b5256db30a18ec010f00bd92cc2a3c855deb8df7844a";
    public static final String RIGHT_KEY = "dae98ccf32a39ae7c7dabf2d3b129a3705e9252f4dc5a5fa25b00e65e4440aa52ed91543ca5dcf1695a0afb619b3d12984d7126532a6bc00c77d803386f0b70f159e5b6697b51a8650c4c1c150b2c3fcb61ed4d807d8e48a2997263352d8d871c443569206bc7e2dab78c719eb322bd76f29f0010e79a7c3f2496c2e6764da5d";
    public static final String DOWN_KEY = "e6e66fb771f5547ecb11fae1c650b5d0c76a567b27218a979c9080be5b51173fd8409e3b89d935f8cda7555fa28d92afa15141fe5537120bc0dab3d645a45c7e4af6fd02fd1587851b576a1c2211fc6a6cba16c463d4a04f689dec1504810efca36ce7430ee40173ec48924636943060c67d00a7480a5cbf0de810a4e74cd885";
    public static final String CENTER_KEY = "7d536df826057c32f0047e01f80ba59bf1c1684d0ff62d3c5f80d80911e518dbff40108b61a583652e21acddb488156e69c70f162da8b6192ea1e9a41c5f9b10413d2a33c050845141b294bee62c93abd801ffa2d5b79e92650c09eecd5f87d6b02148ecc96b15045c585bfc770cc34a29836dcc722ce2bc6d47c871b2ba5db9";

    public static final String BACK_KEY = "4f042bb65b5e8b6fa8f12baf7fc24066c27bd7a8f0f1168451ec5f14b5df9425a0e8986348da4479ef026eb3735356a6e479e3a68af2f114d7500986a4ca89c7a4ab6bcb785d5735c06008d17282989b8cc4df31f2a99ad25ab618994fc73b10ff131fb8441b395cd3353dbccc0f8a3ac036e259a2a2ccddc325cc96db03a2c3";
    public static final String MENU_KEY = "9249e5ec30fbd10f6a09ec6d7a0e0f58ecc979e3a3e3c71203da41d4bccf52053689fe48a601b81163a7d9b2894e3d1b3b626e4a1ed2243f6b515ef8b00f490e59012df119c195c9d1cde27bf6f3eb99cea83742897e429877fa8831f8e029cf75ecf69cbbd9d4a77a16d9286dde6731125aa52cc9b0179f6b1b3a98601e6956";
    public static final String HOME_KEY = "956a261c39075336515cd99417fa387c4db85aacc44a01d8a3214e40d55037c1df34b149288894deeaccd5cddd76eccc25bbf1eeb9de2466f652275c989045090fe556d7768e0708fb966a22f537088072a8a98d5e5dafc490e703d222c8e96611b58990c0754b265dee8d99eb3310d3254ae9c1319d287f4e66e6ee9dc59546";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IRUtils.getInstance(this);

        initViewAndSetListeners();

//        convertMethodTest();
        convertMethodTest1();

        if (IRUtils.getInstance(this).openDevice(getApplicationContext())) {
            Log.v("SADIEYU", "openDevice true");
        } else {
            Log.v("SADIEYU", "openDevice false");
        }

        if (IRUtils.getInstance(this).isOpen()) {
            Log.v("SADIEYU", "isOpen true");
        } else {
            Log.v("SADIEYU", "isOpen false");
        }
    }

    private void initViewAndSetListeners() {
        Button power = (Button) findViewById(R.id.power_off);
        power.setOnClickListener(this);

        Button back = (Button) findViewById(R.id.back);
        back.setOnClickListener(this);

        Button menu = (Button) findViewById(R.id.menu);
        menu.setOnClickListener(this);

        Button home = (Button) findViewById(R.id.home);
        home.setOnClickListener(this);

        Button up = (Button) findViewById(R.id.up);
        up.setOnClickListener(this);

        Button down = (Button) findViewById(R.id.down);
        down.setOnClickListener(this);

        Button left = (Button) findViewById(R.id.left);
        left.setOnClickListener(this);

        Button right = (Button) findViewById(R.id.right);
        right.setOnClickListener(this);

        Button center = (Button) findViewById(R.id.center);
        center.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.power_off:
                sendIrSignal(POWER_KEY);
                break;
            case R.id.back:
                sendIrSignal(BACK_KEY);
                break;
            case R.id.menu:
                sendIrSignal(MENU_KEY);
                break;
            case R.id.home:
                sendIrSignal(HOME_KEY);
                break;
            case R.id.up:
                sendIrSignal(UP_KEY);
                break;
            case R.id.left:
                sendIrSignal(LEFT_KEY);
                break;
            case R.id.down:
                sendIrSignal(DOWN_KEY);
                break;
            case R.id.right:
                sendIrSignal(RIGHT_KEY);
                break;
            case R.id.center:
                sendIrSignal(CENTER_KEY);
                break;
            default:
                break;
        }
    }

    private void sendIrSignal(String key) {


        byte[] b = hexStringToByteArray(key);
        if (IRUtils.getInstance(MainActivity.this).init()) {
            IRUtils.getInstance(MainActivity.this).sendIR(0, b, 0);
            Log.v("SADIEYU", "Library load complete ...");
        } else {
            Log.v("SADIEYU", "Library load not complete ...");
        }
    }

    public String byteToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public void convertMethodTest() {
        String str = "HELLO1231231231231";
        String byte2str = byteToHexString(str.getBytes());
        Log.v("SADIEYU-STR", byte2str);
        byte[] bytes = hexStringToByteArray(byte2str);
        String res = new String(bytes);
        Log.v("SADIEYU-STR", res);
    }

    public void convertMethodTest1() {
        String str = "HELLO1231231231231";
        String byte2str = byteToHexString(str.getBytes());
        Log.v("SADIEYU-STR", byte2str);

        String[] results = bytesToHexStringArray(str.getBytes());

        byte[] bytes = hexStringToByteArray(byte2str);
        String res = new String(bytes);

        Log.v("SADIEYU-STR", results.toString());
    }

    public static String[] bytesToHexStringArray(byte[] src) {
        if (src == null || src.length <= 0) {
            return null;
        }
        String[] str = new String[src.length];

        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                str[i] = "0";
            }
            str[i] = hv;
            System.out.print(hv);
        }
        return str;
    }
}
