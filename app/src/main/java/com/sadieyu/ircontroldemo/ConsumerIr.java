/*
 * Copyright (C) 20013The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sadieyu.ircontroldemo;

// Need the following import to get access to the app resources, since this
// class is in a sub-package.

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.hardware.ConsumerIrManager;
import android.view.View;
import android.widget.TextView;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * App that transmit an IR code
 *
 * <p>This demonstrates the {@link ConsumerIrManager android.hardware.ConsumerIrManager} class.
 *
 * <h4>Demo</h4>
 * Hardware / Consumer IR
 *
 * <h4>Source files</h4>
 * <table class="LinkTable">
 *         <tr>
 *             <td>src/com.example.android.apis/hardware/ConsumerIr.java</td>
 *             <td>Consumer IR demo</td>
 *         </tr>
 *         <tr>
 *             <td>res/any/layout/consumer_ir.xml</td>
 *             <td>Defines contents of the screen</td>
 *         </tr>
 * </table>
 */
public class ConsumerIr extends Activity {
    private static final String TAG = "ConsumerIrTest";
    TextView mFreqsText;
    ConsumerIrManager mCIR;

    public static final String POWER_KEY = "d8b95c9d9fd60c702bd0e3abba5e945f8afca5b13034166f5bf1386f106aa62a33f1af910d6a0b6470a83c637dc52899b61db795429e741c0c0d4d09f66464c07f14997941916fa3eaf797989442d212aa0f4f9433a4d01af19ff1ce7c267a600cf199217983914bd53f491f1fa42f83fb247a15f588dd8817961f0c9ff66c44";
    public static final String LEFT_KEY = "65ab3bf267031c5ae19147c6ab7bdfd0208885f2daf83bc099f60446d9fa72bade79754a517f3c1576ad34139fec0ad1ef815cb6023bc5db8ec9f8303c61b8e60e08c1f29617c902714045ad7b1fa02dbb6a596b8bf07bb15a33bdb201608f7e292d88d0b7252b9109ddf7ff0ac1eda4df2a28171fcbfb353857ebccc1df03ce";
    public static final String UP_KEY = "cde76f5b8880de803e76ba2123ddaf82dfd37a976f0181ef86a36d7e3d5842280553571695ac78bef2ba7f14944e341ea831e38c733450e3ed3771a4c0523fbd10ba459fc8aaba02650b6d14f1889dbda3d7ae16336fe8450e3318cd7c3a2300f301564a420e0e3d16c9b5256db30a18ec010f00bd92cc2a3c855deb8df7844a";
    public static final String RIGHT_KEY = "dae98ccf32a39ae7c7dabf2d3b129a3705e9252f4dc5a5fa25b00e65e4440aa52ed91543ca5dcf1695a0afb619b3d12984d7126532a6bc00c77d803386f0b70f159e5b6697b51a8650c4c1c150b2c3fcb61ed4d807d8e48a2997263352d8d871c443569206bc7e2dab78c719eb322bd76f29f0010e79a7c3f2496c2e6764da5d";
    public static final String DOWN_KEY = "e6e66fb771f5547ecb11fae1c650b5d0c76a567b27218a979c9080be5b51173fd8409e3b89d935f8cda7555fa28d92afa15141fe5537120bc0dab3d645a45c7e4af6fd02fd1587851b576a1c2211fc6a6cba16c463d4a04f689dec1504810efca36ce7430ee40173ec48924636943060c67d00a7480a5cbf0de810a4e74cd885";
    public static final String CENTER_KEY = "7d536df826057c32f0047e01f80ba59bf1c1684d0ff62d3c5f80d80911e518dbff40108b61a583652e21acddb488156e69c70f162da8b6192ea1e9a41c5f9b10413d2a33c050845141b294bee62c93abd801ffa2d5b79e92650c09eecd5f87d6b02148ecc96b15045c585bfc770cc34a29836dcc722ce2bc6d47c871b2ba5db9";

    public static final String BACK_KEY = "4f042bb65b5e8b6fa8f12baf7fc24066c27bd7a8f0f1168451ec5f14b5df9425a0e8986348da4479ef026eb3735356a6e479e3a68af2f114d7500986a4ca89c7a4ab6bcb785d5735c06008d17282989b8cc4df31f2a99ad25ab618994fc73b10ff131fb8441b395cd3353dbccc0f8a3ac036e259a2a2ccddc325cc96db03a2c3";
    public static final String MENU_KEY = "9249e5ec30fbd10f6a09ec6d7a0e0f58ecc979e3a3e3c71203da41d4bccf52053689fe48a601b81163a7d9b2894e3d1b3b626e4a1ed2243f6b515ef8b00f490e59012df119c195c9d1cde27bf6f3eb99cea83742897e429877fa8831f8e029cf75ecf69cbbd9d4a77a16d9286dde6731125aa52cc9b0179f6b1b3a98601e6956";
    public static final String HOME_KEY = "956a261c39075336515cd99417fa387c4db85aacc44a01d8a3214e40d55037c1df34b149288894deeaccd5cddd76eccc25bbf1eeb9de2466f652275c989045090fe556d7768e0708fb966a22f537088072a8a98d5e5dafc490e703d222c8e96611b58990c0754b265dee8d99eb3310d3254ae9c1319d287f4e66e6ee9dc59546";

    /**
     * Initialization of the Activity after it is first created.  Must at least
     * call {@link Activity#setContentView setContentView()} to
     * describe what is to be displayed in the screen.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Be sure to call the super class.
        super.onCreate(savedInstanceState);

        // Get a reference to the ConsumerIrManager
        mCIR = (ConsumerIrManager)getSystemService(Context.CONSUMER_IR_SERVICE);

        // See assets/res/any/layout/consumer_ir.xml for this
        // view layout definition, which is being set here as
        // the content of our screen.
        setContentView(R.layout.consumer_ir);

        // Set the OnClickListener for the button so we see when it's pressed.
        findViewById(R.id.send_button).setOnClickListener(mSendClickListener);
        findViewById(R.id.get_freqs_button).setOnClickListener(mGetFreqsClickListener);
        mFreqsText = (TextView) findViewById(R.id.freqs_text);
    }

    View.OnClickListener mSendClickListener = new View.OnClickListener() {
        @TargetApi(Build.VERSION_CODES.KITKAT)
        public void onClick(View v) {
            if (!mCIR.hasIrEmitter()) {
                Log.e(TAG, "No IR Emitter found\n");
                return;
            }

            // A pattern of alternating series of carrier on and off periods measured in
            // microseconds.
//            int[] pattern = {1901, 4453, 625, 1614, 625, 1588, 625, 1614, 625, 442, 625, 442, 625,
//                468, 625, 442, 625, 494, 572, 1614, 625, 1588, 625, 1614, 625, 494, 572, 442, 651,
//                442, 625, 442, 625, 442, 625, 1614, 625, 1588, 651, 1588, 625, 442, 625, 494, 598,
//                442, 625, 442, 625, 520, 572, 442, 625, 442, 625, 442, 651, 1588, 625, 1614, 625,
//                1588, 625, 1614, 625, 1588, 625, 48958};

            byte[] bytes = hexStringToByteArray(POWER_KEY);
            int[]  parrern = byteArr2IntArry(bytes);
            // transmit the pattern at 38.4KHz
            mCIR.transmit(1800, parrern);
        }
    };

    View.OnClickListener mGetFreqsClickListener = new View.OnClickListener() {
        @TargetApi(Build.VERSION_CODES.KITKAT)
        public void onClick(View v) {
            StringBuilder b = new StringBuilder();

            if (!mCIR.hasIrEmitter()) {
                mFreqsText.setText("No IR Emitter found!");
                Log.e(TAG, "No IR Emitter found!\n");
                return;
            }

            // Get the available carrier frequency ranges
            ConsumerIrManager.CarrierFrequencyRange[] freqs = mCIR.getCarrierFrequencies();
            b.append("IR Carrier Frequencies:\n");
            for (ConsumerIrManager.CarrierFrequencyRange range : freqs) {
                b.append(String.format("    %d - %d\n", range.getMinFrequency(),
                            range.getMaxFrequency()));
            }
            mFreqsText.setText(b.toString());
        }
    };

    public String byteToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    byte[] integersToBytes(int[] values)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        for(int i=0; i < values.length; ++i)
        {
            try {
                dos.writeInt(values[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return baos.toByteArray();
    }

    int[] byteArr2IntArry(byte[] byteArray) {
        return ByteBuffer.wrap(byteArray).asIntBuffer().array();
    }


    public static String[] bytesToHexStringArray(byte[] src){
        if (src == null || src.length <= 0) {
            return null;
        }
        String[] str = new String[src.length];

        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                str[i] = "0";
            }
            str[i] = hv;
        }
        return str;
    }
}
